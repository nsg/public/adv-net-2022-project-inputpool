group_id,traffic_present,traffic_valid,failure_present,failure_valid
00_baseline,True,True,True,True
01_turing,False,False,False,False
02_wirth,False,False,False,False
03_shannon,False,False,False,False
04_cerf,False,False,False,False
05_dijkstra,False,False,False,False
06_hamilton,False,False,False,False
07_boole,False,False,False,False
08_hopper,False,False,False,False
09_kahn,False,False,False,False
10_knuth,False,False,False,False
11_lamport,False,False,False,False
13_rexford,False,False,False,False
14_allen,False,False,False,False
15_berners-lee,False,False,False,False
16_shenker,False,False,False,False
17_stoica,False,False,False,False
