group_id,traffic_present,traffic_valid,failure_present,failure_valid
00_baseline,True,True,True,True
01_turing,False,False,True,False
02_wirth,True,True,True,True
03_shannon,False,False,False,False
04_cerf,True,True,True,True
05_dijkstra,True,True,True,True
06_hamilton,True,True,True,True
07_boole,True,False,True,False
08_hopper,True,True,True,True
09_kahn,True,True,True,True
10_knuth,True,True,True,True
11_lamport,False,False,False,False
13_rexford,False,False,False,False
14_allen,True,True,True,True
15_berners-lee,False,False,False,False
16_shenker,True,True,True,True
17_stoica,False,False,False,False
